# build stage
FROM node:12 as build-stage

LABEL maintainer=1309701492@qq.com

WORKDIR /app
COPY . .
RUN yarn install --registry=https://registry.npm.taobao.org

RUN npm run build

# production stage
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 80 
CMD ["nginx", "-g", "daemon off;"]